// Script of the Create Database Window
const path = require("path")

module.exports = {
    // If you extension is a theme, you can include it's style file (can be included only by the File Tab)
    // please note: Any file path included, must be in absolute form
    "style": path.join(__dirname, "../css/createWindow.css")
}