const {remote, ipcRenderer} = require("electron");
const path = require("path")
const fs = require("fs")
require("./components.js")

window.windowName = "mainWindow"

window.sql = remote.getGlobal("sql")

if (!remote.getGlobal("sql").isOpen()) {
    fs.exists(remote.getGlobal("config").databasePath, (exists) => {
        let redirect
        if (exists) {
            redirect = path.join(
                remote.app.getAppPath(), "components/html/auth.html"
            );
        } else {
            redirect = path.join(
                remote.app.getAppPath(), "components/html/create.html"
            );
        }
        window.location.href = redirect;
    }) 
}

function dialogBox(content, closeTime = 15000) {
    let newDialog = $(`
        <div class="dialog-box">
            <button class="db-close"></button>
            <span class="db-closetimer"></span>
        </div>
    `)
    newDialog.append(content)
    $("#dialog-box-list").append(newDialog)
    $(".db-close").click(function() {
        newDialog.find(".db-closetimer").stop()
        newDialog.fadeOut(500, function() {
            $(this).remove()
        })
    })
    if (closeTime >= 0) {
        newDialog.find(".db-closetimer").animate({
            height: 0,
        }, closeTime, "linear", function() {
            newDialog.fadeOut(500, function() {
                $(this).remove()
            })
        })
    } else {
        newDialog.find(".db-closetimer").remove()
    }
}

ipcRenderer.on('dialogBox', (ev, content, closeTime) => {
    dialogBox(content, closeTime)
})

window.extensionReload()

$(".sb-tab").on('click', function() {
    if ($(this).index() == $(".sb-tab.current").index()) {
        return;
    }
    $(".sb-tab.current").removeClass("current")
    $(this).addClass("current")
    $(".tab-view.current").removeClass("current")
    $(`.tab-view:nth-child(${$(this).index()+1})`).addClass("current")
})