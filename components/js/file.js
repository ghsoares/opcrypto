const {remote, ipcRenderer} = require("electron");
const {ContextMenu, FileExplorer} = require("./components.js");
const path = require("path")
const srs = require("secure-random-string")
const ect = require("encryptools")
//remote.getCurrentWebContents().openDevTools();

window.windowName = "fileTab"

let ctx = new ContextMenu()
let fe = new FileExplorer(ctx)

fe.blockedPaths.push(remote.app.getAppPath())

let fileQueries = {}

fe.onItemExplorerCreate = function(item, filePath) {
    let eFiles = remote.getGlobal("sql").get("encryptedFiles") || {}
    if (fileQueries[filePath]) {
        $(item).addClass("fe-filequerried")
    }
    if (eFiles[filePath]) {
        $(item).addClass("fe-fileencrypted")
    }
    return item;
}

fe.contextMenu = function(fileStats, filePath, ev) {
    if (fileStats.isFile()) {
        let eFiles = remote.getGlobal("sql").get("encryptedFiles") || {}
        if (fileQueries[fileQueries]) {
            return;
        }
        let menu
        if (eFiles[filePath]) {
            menu = $(`
            <ctx-root>
                <ctx-option id="encrypt">Decrypt</ctx-option>
            </ctx-root>
            `)
            menu.find('#encrypt')[0].click = function () {
                appendFileToEncryptQuerry(filePath, ev, "decrypt")
                return true;
            }
            this.ctxmenu.open(menu, ev)
        } else {
            menu = $(`
            <ctx-root>
                <ctx-option id="encrypt">Encrypt</ctx-option>
            </ctx-root>
            `)
            menu.find('#encrypt')[0].click = function () {
                appendFileToEncryptQuerry(filePath, ev, "encrypt")
                return true;
            }
            this.ctxmenu.open(menu, ev)
        }
    } else if (fileStats.isDirectory()) {
        let menu = $(`
        <ctx-root>
            <ctx-option id="open">Open</ctx-option>
        </ctx-root>
        `)
        menu.find('#open')[0].click = function () {
            $(ev.currentTarget).dblclick()
            return true;
        }
        this.ctxmenu.open(menu, ev)
    }
}

fe.jcode.find(".fe-topbar").append($(`
<div class="fe-filequeries">
<button class="button fe-filequeriesaccept">Start</button>
</div>
`))

$("#main").append(fe.jcode)
$("#main").append(ctx.jcode)

function error(msg) {
    let content = `<span>${msg}</span>`
    ipcRenderer.send("dialogBox", content)
}

function success(msg) {
    let content = `<span>${msg}</span>`
    ipcRenderer.send("dialogBox", content)
}

function encryptDecryptQuery() {
    let completed = remote.getGlobal("sql").get("encryptedFiles") || {}
    for (let q of Object.values(fileQueries)) {
        q["querryEl"].find(".fe-filequeryremove").remove()
    }
    return new Promise(function(resolve, reject) {
        let next = function() {
            if (Object.keys(fileQueries)[0] == undefined) {
                resolve()
                return;
            }
            let path = Object.keys(fileQueries)[0]
            let querryEl = fileQueries[path].querryEl
            let fileEl = fileQueries[path].fileEl
            let action = fileQueries[path].action
            if (action == "encrypt") {
                let key = srs({length: 128})
                ect.encryptFile(path, key, path, (processed, total) => {
                    let progress = (processed / total) * 100.0
                    querryEl.find(".fe-filequeryprogress").css("width", progress + "%")
                })
                .then(() => {
                    querryEl.find(".fe-filequeryprogress").css("width", "100%")
                    completed[path] = key
                    delete fileQueries[path]
                    querryEl.fadeOut(250, "linear", function() {
                        querryEl.remove()
                        fileEl.removeClass("fe-filequerried")
                        fileEl.addClass("fe-fileencrypted")
                        next()
                    })
                })
                .catch((err) => {
                    error(`
                        There was an error while trying to encrypt the file [${path}]: <br>
                        "${err}"
                    `)
                })
            } else {
                let key = completed[path]
                ect.decryptFile(path, key, path, (processed, total) => {
                    let progress = (processed / total) * 100.0
                    querryEl.find(".fe-filequeryprogress").css("width", progress + "%")
                })
                .then(() => {
                    querryEl.find(".fe-filequeryprogress").css("width", "100%")
                    completed[path] = undefined
                    delete fileQueries[path]
                    querryEl.fadeOut(250, "linear", function() {
                        querryEl.remove()
                        fileEl.removeClass("fe-filequerried")
                        fileEl.removeClass("fe-fileencrypted")
                        next()
                    })
                })
                .catch((err) => {
                    error(`
                        There was an error while trying to decrypt the file [${path}]: <br>
                        "${err}"
                    `)
                })
            }
        }
        next()
    }).then(() => {
        remote.getGlobal("sql").set("encryptedFiles", completed)
    })
}

fe.jcode.find(".fe-filequeriesaccept").click(function() {
    encryptDecryptQuery().then(() => {
        success("All queried files are encrypted/decrypted!")
    })
})

function appendFileToEncryptQuerry(filePath, ev, fileAction) {
    let file = $(`
    <div class="fe-filequery">
    <span class="fe-filequeryprogress"></span>
    ${path.basename(filePath)}
    <button class="fe-filequeryremove"></button>
    </div>
    `)
    file.find(".fe-filequeryremove").click(function() {
        file.find(".fe-filequeryremove").attr("disabled", true)
        delete fileQueries[filePath];
        file.fadeOut(250, "linear", function() {
            file.remove()
        })
    })
    file.hide().fadeIn(250)
    fe.jcode.find(".fe-filequeries").prepend(file)
    fileQueries[filePath] = {
        action: fileAction,
        querryEl: file,
        fileEl: $(ev.currentTarget)
    }
    $(ev.currentTarget).addClass("fe-filequerried")
}

window.extensionReload()