const $ = require("jquery")
window.$ = window.jQuery = require('jquery');
window.reload = function() {
    window.location.href = window.location.href;
}
window.windowName = ""
const {
    remote,
    ipcRenderer
} = require("electron");

const fs = require("fs");
const path = require("path");

let timeout

let requiredFields = $('input[required]')

let lastClickedEl

require("jquery-ui-dist/jquery-ui");

$("#tb-window-minimize").on('click', () => {
    remote.getCurrentWindow().minimize()
})

$("#tb-window-maximize").on('click', () => {
    remote.getCurrentWindow().maximize()
    $("#tb-window-maximize").addClass("hidden")
    $("#tb-window-restore").removeClass("hidden")
})

$("#tb-window-restore").on('click', () => {
    remote.getCurrentWindow().restore()
    $("#tb-window-restore").addClass("hidden")
    $("#tb-window-maximize").removeClass("hidden")
})

$("#tb-window-close").on('click', () => {
    remote.getCurrentWindow().close()
})

$(".form").has('input[class^="form-input"][required]').find(".form-submit").attr("disabled", true)

$(".form").submit(function() {
    return false;
})

$(".form").find('input[type="range"]').each(function( _ , el) {
    let id = $(el).attr('id')
    $(el).on('input change', function() {
        $(".form").find(`label[for="${id}"].value`).html($(el).val())
    })
    $(".form").find(`label[for="${id}"].value`).html($(el).val())
})

$(".acc-valueaction.view").click(function() {
    let target = $('#' + $(this).attr("for"))
    if (target.attr("type") == "password") target.attr("type", "text")
    else target.attr("type", "password")
})

function copyToClipboard(text, timeout = 0, dialog = true) {
    let temp = $(`<input></input>`)
    $("body").append(temp);
    
    temp.val(text).select();
    document.execCommand("copy");
    temp.remove();

    if (timeout) clearTimeout(timeout)

    if (timeout > 0) {
        if (dialog) {
            ipcRenderer.send('dialogBox', `<span>Copied text will be cleared in ${timeout / 1000} seconds!</span>`, timeout)
        }
        timeout = setTimeout(() => {
            copyToClipboard(' ', 0, false)
        }, timeout)
    } else {
        if (dialog) {
            ipcRenderer.send('dialogBox', `<span>Text copied!</span>`, 500)
        }
    }
}

$(".acc-valueaction.copy").click(function() {
    let target = $('#' + $(this).attr("for"))
    let text;
    if (target.is('input')) {
        text = target.val()
    } else {
        text = target.html()
    }
    if ($(this).hasClass('temp')) {
        copyToClipboard(text, 5000)
    } else {
        copyToClipboard(text)
    }
})

function checkFormFilled(form) {
    let filled = true;
    $(form).find('input[required]').each(function() {
        if ($(this).val() == '') {
            filled = false
        }
    })
    return filled;
}

requiredFields.each(function(index, el) {
    let filled = checkFormFilled($(this.form))
    $(this.form).find("input[type='submit']").attr("disabled", !filled)
})

requiredFields.keyup(function() {
    let filled = checkFormFilled($(this.form))
    $(this.form).find("input[type='submit']").attr("disabled", !filled)
})

requiredFields.change(function() {
    let filled = checkFormFilled($(this.form))
    $(this.form).find("input[type='submit']").attr("disabled", !filled)
})

function searchElUpdate(search) {
    search.each(function(_,el) {
        let currentOption = 0;
        let options = $(this).find('div').children().not('.default').not('span')

        $(this).find("input").focus(function() {
            $(this).trigger("keyup")
            $(".option.selected").removeClass("selected")
            $(options[currentOption]).addClass("selected")
        })

        $(this).find("input").keyup(function(ev) {
            let query = $(this).val()
            let finded = false
            for (let option of options) {
                let text = $(option).text()
                let regex = new RegExp(query, "i")
                if (text.search(regex) == -1) {
                    $(option).css("display", "none")
                } else {
                    $(option).css("display", "block")
                    finded = true
                }
            }
            if (!finded) {
                $(this).next('div').find('.default').css("display", "block")
            } else {
                $(this).next('div').find('.default').css("display", "none")
            }
        })
        
        $(this).find("input").blur(function(ev) {
            currentOption = 0
        })
        
        $(document).mousedown(function(ev) {
            lastClickedEl = $(ev.target)
            if ($(el).find(lastClickedEl).length != 0) {
                if (lastClickedEl.is(".option")) {
                    if (lastClickedEl.is(".default")) return;
                    $(el).find("input").val(lastClickedEl.text().split(" ")[0])
                    $(el).find("input").trigger("keyup")
                }
            }
        })
    })
}

function resetForm($form) {
    $form.find('input')
    .not(':button, :submit, :reset, :hidden')
    .val('')
    .removeAttr('checked')
    .removeAttr('selected');
}

class ContextMenu {
    constructor() {
        this.jcode = $(`<div class="ctx-menu hidden"></div>`);
        this.code = this.jcode[0];
    }

    syntaxCreator(elTree) {
        var self = this;
        let code = $(`<div class="ctx-menu hidden"></div>`)
        if ($(elTree).is('ctx-root')) {
            code.append($(elTree).children())
        } else {
            code.append($(elTree))
        }

        function recursive(el) {
            for (let child of $(el).children()) {
                if ($(child).is('ctx-option')) {
                    let clickEvent = $(child)[0].click
                    let content = $(child).html()
                    let childCode = $(`
                        <button class="ctx-option">${content}</button>
                    `);
                    childCode.click(function () {
                        if (!clickEvent) {
                            self.close()
                        } else if (clickEvent()) self.close()
                    })
                    $(child).replaceWith(childCode)
                }
                if ($(child).is('ctx-separator')) {
                    let childCode = `
                        <span class="ctx-separator"></span>
                    `
                    $(child).replaceWith(childCode)
                }
                if ($(child).is('ctx-expandable')) {
                    let name = ""

                    if (child.childNodes) {
                        name = child.childNodes[0].nodeValue
                    }

                    name = name.trim()

                    let content = $(child).children()
                    if (content) {
                        recursive($(child))
                    }
                    content = $(child).children()

                    let childCode = $(`
                        <div class="ctx-expand">
                            <button class="ctx-option ctx-openexpand">${name}</button>
                            <div class="ctx-expandcontent hidden">
                            </div>
                        </div>
                    `);
                    childCode.find(".ctx-expandcontent").append(content)
                    $(childCode.find(".ctx-openexpand")[0]).click(function () {
                        $(childCode.find(".ctx-expandcontent")[0]).toggleClass("hidden")
                    })
                    $(child).replaceWith(childCode)
                }
            }
        }

        recursive(code)
        this.jcode.append(code.children())
    }

    open(menuTree = '', ev) {
        this.jcode.html("")
        this.syntaxCreator(menuTree)
        this.jcode.removeClass("hidden")

        let posx = 0;
        let posy = 0;

        posx = ev.clientX
        posy = ev.clientY - this.code.clientHeight

        this.jcode.css("left", posx + "px").css("top", posy + "px")

        let click = (ev) => {
            if (this.jcode.has(ev.target).length > 0 || this.code == ev.target) {
                return;
            }
            this.close(ev)
            $(document).off('click', click)
        }
        $(document).on('click', click)
    }

    close() {
        this.jcode.addClass("hidden")
    }
}

class FileExplorer {
    constructor(ctxmenu = new ContextMenu(), currentLocation = require("os").homedir()) {
        var self = this;

        let gridIcon = path.join(__dirname, "../icons/grid-icon.png")
        let listIcon = path.join(__dirname, "../icons/list-icon.png")

        /* #region jquery code */
        this.jcode = $(`
        <div class="file-explorer">
            <div class="fe-topbar">
                <div class="fe-guioptions">
                    <button class="fe-show fe-grid">
                        <img class="fe-showicon" src="${gridIcon}"/>
                    </button>
                    <button class="fe-show fe-list">
                        <img class="fe-showicon" src="${listIcon}"/>
                    </button>
                </div>
                <div class="fe-navigation">
                    <div class="fe-backnext">
                        <button class="fe-back"><</button>
                        <button class="fe-next">></button>
                    </div>
                    <div class="fe-location">
                        <span class="fe-locationfield"></span>
                        <input type="text" class="fe-locationinput">
                    </div>
                    <span class="fe-wronglocation">This path doesn't exist</span>
                </div>
            </div>
            <div class="fe-explorer fe-list">
                <div class="fe-orderoptions">
                    <span class="fe-order fe-ordername">
                        <span class="fe-ordercontent">Name</span>
                    </span>
                    <span class="fe-order fe-orderdate">
                        <span class="fe-ordercontent">Date Modified</span>
                    </span>
                    <span class="fe-order fe-ordertype">
                        <span class="fe-ordercontent">Type</span>
                    </span>
                </div>
                <div class="fe-explorerfiles">
                </div>
            </div>
        </div>
        `);
        /* #endregion */

        this.code = this.jcode[0];

        this.currentLocation = currentLocation;
        this.locationHistory = [];
        this.blockedPaths = [];

        let pathParts = this.pathSplitted(currentLocation)
        for (let i = 0; i < pathParts.length - 1; i++) {
            this.locationHistory.push(path.join.apply(null, pathParts.slice(0, i + 1)))
        }

        this.jcode.find(".fe-back").click(function () {
            self.back()
        })
        this.jcode.find(".fe-next").click(function () {
            self.next()
        })

        this.jcode.find(".fe-show.fe-grid").click(function () {
            self.showGrid()
        })
        this.jcode.find(".fe-show.fe-list").click(function () {
            self.showList()
        })

        this.jcode.find(".fe-locationinput").focus(function () {
            $(this).val(self.currentLocation)
            self.jcode.find(".fe-locationfield").children().css("display", "none")
            $(this).addClass("fe-editing")
            $(this).keyup((ev) => {
                if (ev.keyCode == 13) {
                    $(this).blur()
                    $(this).removeClass("fe-editing")
                    self.jcode.find(".fe-locationfield").children().css("display", "initial")
                    self.openLocation($(this).val())
                }
            })
            self.jcode.find(".fe-locationinput").focusout(function () {
                $(this).removeClass("fe-editing")
                self.jcode.find(".fe-locationfield").children().css("display", "initial")
            })
        })

        this.jcode.find('.fe-ordername').resizable({
            handles: 'e',
        })
    
        this.jcode.find('.fe-orderdate').resizable({
            handles: 'e',
        })
    
        this.jcode.find('.fe-ordertype').resizable({
            handles: 'e',
        })

        this.locationHistoryIdx = this.locationHistory.length - 1;

        this.openLocation(currentLocation);

        this.ctxmenu = ctxmenu
    }

    getIcon(filePath) {
        return new Promise(resolve => {
            remote.app.getFileIcon(filePath, {
                size: "normal"
            }).then((icon) => {
                let img = $(`<img class="fe-fileicon"/>`)[0];
                img.src = icon.toDataURL();
                resolve(img);
            });
        });
    }

    contextMenu(fileStats, filePath, ev) {
        if (fileStats.isFile()) {
            let menu = $(`
            <ctx-root>
                <ctx-option id="encrypt">Encrypt</ctx-option>
                <ctx-separator></ctx-separator>
                <ctx-option id="properties">Properties</ctx-option>
            </ctx-root>
            `)
            menu.find('#encrypt')[0].click = function () {
                console.log(`Encrypting (${filePath}) file!`)
                return true;
            }
            menu.find('#properties')[0].click = function () {
                console.log(`(${filePath}) properties`)
                return true;
            }
            this.ctxmenu.open(menu, ev)
        } else if (fileStats.isDirectory()) {
            let menu = $(`
            <ctx-root>
                <ctx-option id="open">Open</ctx-option>
            </ctx-root>
            `)
            menu.find('#open')[0].click = function () {
                $(ev.currentTarget).dblclick()
                return true;
            }
            this.ctxmenu.open(menu, ev)
        }
    }

    onItemExplorerCreate(item, filePath) {
        return item;
    }

    createExplorerItem(filePath = "") {
        var self = this;
        let folderIcon = path.join(__dirname, "../icons/folder-icon.png")
        return new Promise(
            resolve => {
                fs.stat(filePath, (err, stats) => {
                    if (stats.isFile()) {
                        self.getIcon(filePath).then((img) => {
                            let day = stats.ctime.getDay()
                            let month = stats.ctime.getMonth() + 1
                            let year = stats.ctime.getFullYear()

                            if (day < 10) day = '0' + day
                            if (month < 10) month = '0' + month

                            let ctime = `${day}/${month}/${year}`
                            let itemEl = $(`
                            <div class="fe-file">
                                <div class="fe-fcontent">
                                    <span class="fe-filedetail fe-filename">
                                        <span class="fe-fdcontent">
                                            ${path.basename(filePath)}
                                        </span>
                                        <span class="fe-fdborder"></span>
                                    </span>
                                    <span class="fe-filedetail fe-filedate">
                                        <span class="fe-fdcontent">${ctime}</span>
                                        <span class="fe-fdborder"></span>
                                    </span>
                                    <span class="fe-filedetail fe-filetype">
                                        <span class="fe-fdcontent">Plain Text File</span>
                                        <span class="fe-fdborder"></span>
                                    </span>
                                </div>
                            </div>
                            `)
                            itemEl.filepath = filePath
                            itemEl.find(".fe-filename>.fe-fdcontent").prepend(img)
                            itemEl.contextmenu(function (ev) {
                                self.contextMenu(stats, filePath, ev)
                            })

                            itemEl = self.onItemExplorerCreate(itemEl, filePath)

                            resolve(itemEl)
                        })
                    } else {
                        let day = stats.ctime.getDay()
                        let month = stats.ctime.getMonth() + 1
                        let year = stats.ctime.getFullYear()

                        if (day < 10) day = '0' + day
                        if (month < 10) month = '0' + month

                        let ctime = `${day}/${month}/${year}`
                        let itemEl = $(`
                        <div class="fe-folder">
                            <div class="fe-fcontent">
                                <span class="fe-folderdetail fe-foldername">
                                    <span class="fe-fdcontent">
                                        <img class="fe-foldericon" src="${folderIcon}"/>${path.basename(filePath)}
                                    </span>
                                    <span class="fe-fdborder"></span>
                                </span>
                                <span class="fe-folderdetail fe-folderdate">
                                    <span class="fe-fdcontent">${ctime}</span>
                                    <span class="fe-fdborder"></span>
                                </span>
                                <span class="fe-folderdetail fe-foldertype">
                                    <span class="fe-fdcontent">File Folder</span>
                                    <span class="fe-fdborder"></span>
                                </span>
                            </div>
                        </div>
                        `)
                        itemEl.filepath = filePath
                        itemEl.dblclick(function () {
                            self.openLocation(filePath)
                        })
                        itemEl.contextmenu(function (ev) {
                            self.contextMenu(stats, filePath, ev)
                        })

                        itemEl = self.onItemExplorerCreate(itemEl, filePath)

                        resolve(itemEl)
                    }
                })
            }
        )
    }

    pathSplitted(p = "") {
        let splitted = p.split(path.sep)
        if (splitted[0] == "") {
            splitted[0] = "/"
        }
        return splitted
    }

    isRelative(child, parent) {
        if (child === parent) return true
        const parentTokens = parent.split('/').filter(i => i.length)
        return parentTokens.every((t, i) => child.split('/')[i] === t)
    }

    listDir(p = "") {
        let explorerEl = this.jcode.find(".fe-explorerfiles")
        explorerEl.html("")

        fs.readdir(p, (err, files) => {
            let queries = []
            for (let file of files) {
                let isBlocked = false
                for (let blockedPath of this.blockedPaths) {
                    if (this.isRelative(path.join(p, file), blockedPath)) {
                        isBlocked = true;
                        break;
                    }
                }
                if (isBlocked) continue;
                let fullpath = path.join(p, file)
                queries.push(
                    this.createExplorerItem(fullpath).then((itemEl) => {
                        explorerEl.append(itemEl)            
                    })
                )
            }
            Promise.all(queries).then(() => {
                this.jcode.find('.fe-ordername').resizable(
                    "option", "alsoResize",
                    explorerEl.find('.fe-foldername, .fe-filename')
                )

                this.jcode.find('.fe-orderdate').resizable(
                    "option", "alsoResize",
                    explorerEl.find('.fe-folderdate, .fe-filedate')
                )

                this.jcode.find('.fe-ordertype').resizable(
                    "option", "alsoResize",
                    explorerEl.find('.fe-foldertype, .fe-filetype')
                )

                explorerEl.find('.fe-foldername, .fe-filename').css("width",
                    this.jcode.find('.fe-ordername').css("width")
                )
                explorerEl.find('.fe-folderdate, .fe-filedate').css("width",
                    this.jcode.find('.fe-orderdate').css("width")
                )
                explorerEl.find('.fe-foldertype, .fe-filetype').css("width",
                    this.jcode.find('.fe-ordertype').css("width")
                )
            })
        })

        /*
        this.jcode.find('.fe-orderdate').resizable(
            "option", "alsoResize",
            this.jcode.find('.fe-folderdate,.fe-filedate')
        );

        this.jcode.find('.fe-ordetype').resizable(
            "option", "alsoResize",
            this.jcode.find('.fe-foldertype,.fe-filetype')
        );*/
    }

    openLocation(location = "", appendHistory = true) {
        for (let blockedPath of this.blockedPaths) {
            if (this.isRelative(location, blockedPath)) {
                console.log(location, " Is blocked as is or is relative to the path: ", blockedPath)
                return;
            }
        }
        fs.stat(location, (err, stats) => {
            if (err) {
                if (err.code == 'ENOENT') {
                    this.jcode.find('.fe-location').css("border-color", "red")
                    this.jcode.find('.fe-wronglocation').css("opacity", "1")
                    this.jcode.find('.fe-location').on('transitionend', () => {
                        setTimeout(() => {
                            this.jcode.find('.fe-location').css("border-color", "initial")
                            this.jcode.find('.fe-wronglocation').css("opacity", "0")
                        }, 500)
                    })
                }
                return false;
            }
            this.currentLocation = location;

            this.ctxmenu.close()

            if (appendHistory) {
                this.locationHistory.push(location)
                this.locationHistoryIdx = this.locationHistory.length - 1
            }

            if (this.locationHistoryIdx == 0) {
                this.jcode.find(".fe-back").attr("disabled", true)
            } else {
                this.jcode.find(".fe-back").attr("disabled", false)
            }

            if (this.locationHistoryIdx == this.locationHistory.length - 1) {
                this.jcode.find(".fe-next").attr("disabled", true)
            } else {
                this.jcode.find(".fe-next").attr("disabled", false)
            }

            this.updatePath()
            this.listDir(location)
        })
    }

    updatePath() {
        var self = this;

        let locationEl = this.jcode.find(".fe-location")
        let locationFieldEl = this.jcode.find(".fe-locationfield")

        locationFieldEl.html("")

        let currentLocationSplitted = this.pathSplitted(this.currentLocation)

        for (let pathPartIdx = 0; pathPartIdx < currentLocationSplitted.length; pathPartIdx++) {
            let locationFolderEl = $(`
                <button class="fe-locationfolder">${currentLocationSplitted[pathPartIdx]}</button>
            `)
            locationFieldEl.append(locationFolderEl);
            locationFolderEl.on('click', function () {
                let thisLocation = path.join.apply(
                    null,
                    currentLocationSplitted.slice(0, pathPartIdx + 1)
                )
                self.openLocation(thisLocation)
            })
            if (pathPartIdx < currentLocationSplitted.length - 1) {
                locationFieldEl.append(`
                    <span class="fe-locationseparator">></span>
                `);
            }
        }
        
        locationEl.scrollLeft(locationFieldEl[0].clientWidth)

        let event = new CustomEvent('updatedPath', {'detail': {
            'newPath': this.currentLocation
        }})
        this.code.dispatchEvent(event)
    }

    back() {
        this.locationHistoryIdx -= 1;
        this.openLocation(this.locationHistory[this.locationHistoryIdx], false)
    }

    next() {
        this.locationHistoryIdx += 1;
        this.openLocation(this.locationHistory[this.locationHistoryIdx], false)
    }

    showGrid() {
        this.jcode.find(".fe-explorer").removeClass("fe-list");
        this.jcode.find(".fe-explorer").addClass("fe-grid");
    }

    showList() {
        this.jcode.find(".fe-explorer").removeClass("fe-grid");
        this.jcode.find(".fe-explorer").addClass("fe-list");
    }
}

window.extensionReload = function() {
    let activeExtensions = remote.getGlobal("config").activeExtensions
    let extensions = remote.getGlobal("extensions")
    console.log(window.windowName)
    for (let activeExtension of activeExtensions) {
        let thisExtension = require(
            extensions[activeExtension][window.windowName]
        )
        let style = thisExtension.style
        if (style) {
            let newStylesheet = $(`<link rel="stylesheet" href="${style}" />`)
            $('head').append(newStylesheet)
        }
    }
}

module.exports = {
    searchElUpdate,
    resetForm,
    ContextMenu,
    FileExplorer
}