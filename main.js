const {
    app,
    ipcMain,
    BrowserWindow
} = require("electron")

const fs = require("fs")

const path = require("path")

const chokidar = require("chokidar")

let win;

global.sql = require("./components/js/sql.js")
global.config = {}
global.extensions = {}

ipcMain.on('dialogBox', (ev, content, closeTime) => {
    win.send('dialogBox', content, closeTime)
})

ipcMain.on('configChanged', (ev, write = true) => {
    config_changed(write)
})

ipcMain.on('windowReload', () => {
    win.reload()
})

function config_changed(write = true) {
    if (process.mainModule.filename.indexOf('app.asar') === -1) {
        global.config.rootFolder = global.config.rootFolder.replace("./", app.getAppPath())
    } else {
        global.config.rootFolder = global.config.rootFolder.replace("./", path.join(app.getAppPath(), "../../"))
    }

    global.config.databasePath = path.join(global.config.rootFolder, "database.db")
    global.config.extensionsFolder = path.join(global.config.rootFolder, "extensions")

    !fs.existsSync(global.config.extensionsFolder) && fs.mkdirSync(global.config.extensionsFolder)

    if (write) fs.writeFile("config.json", JSON.stringify(global.config, null, 2), () => {})
}

function loadConfig() {
    return new Promise(function(resolve) {
        fs.exists("config.json", (exists) => {
            if (exists) {
                fs.readFile("config.json", "utf8", (err, data) => {
                    global.config = JSON.parse(data)
                    config_changed(false)
                    resolve()
                })
            } else {
                global.config = {
                    "firstName": "",
                    "lastName": "",
                    "email": "",
                    "rootFolder": "./",
                    "databasePath": "",
                    "extensionsFolder": "",
                    "activeExtensions": []
                }
                config_changed()
                resolve()
            }
        })
    })
}

function createWindow() {
    win = new BrowserWindow({
        width: 1024,
        height: 600,
        minWidth: 512,
        minHeight: 300,
        frame: false,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
        }
    });

    fs.exists(global.config.databasePath, (exists) => {
        if (exists) {
            win.loadFile("./components/html/auth.html");
        } else {
            win.loadFile("./components/html/create.html");
        }

        win.on('closed', () => {
            win = null;
        });

        win.webContents.openDevTools()
    })
}

function loadExtensions() {
    global.extensions = {}
    return new Promise((resolve, _) => {
        let dir = global.config.extensionsFolder
        let extensions = fs.readdirSync(dir)
        for (let extension of extensions) {
            let mainFile = path.join(dir, extension, "extension.js")
            if (fs.existsSync(mainFile)) {
                let extensionObj = require(mainFile)
                if (extensionObj.name) {
                    if (extensionObj.type != "Template") {
                        for (let key of Object.keys(extensionObj)) {
                            extensionObj[key] = extensionObj[key].replace("./", path.join(dir, extension) + "\\")
                        }
                        let add = 0
                        while (true) {
                            if (add == 0) {
                                if (global.extensions[extensionObj.name]) {
                                    add += 1
                                } else {
                                    global.extensions[extensionObj.name] = extensionObj
                                    break
                                }
                            } else {
                                if (global.extensions[extensionObj.name+add]) {
                                    add += 1
                                } else {
                                    global.extensions[extensionObj.name+add] = extensionObj
                                    break
                                }
                            }
                        }
                    }
                }
            } else {
                continue;
            }
        }
        for (let activeExtension of global.config.activeExtensions) {
            let index = global.config.activeExtensions.indexOf(activeExtension)
            if (!global.extensions[activeExtension]) {
                global.config.activeExtensions.splice(index, 1)
                config_changed()
            }
        }
        resolve()
    })
}

app.on('ready', () => {
    loadConfig().then(() => {
        loadExtensions().then(() => {
            createWindow();
            let watcher = chokidar.watch('./extensions')
            watcher.on('change', (event, path) => {
                loadExtensions().then(() => {
                    win.reload()
                })
            })
        })
    })
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})