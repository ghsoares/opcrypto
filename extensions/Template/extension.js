// Here you can access the main script and return the
// extension informations

module.exports = {
    // Some basic info
    "name": "[Name of the extension]",
    "author": "[Author of the extension]",
    "type": "Template", // Ignored extension, used only as example for other extensions
    "description": "Default Template extension",
    "thumbnail": "[path/to/thumbnail]",
    // Default files, please don't change these
    "mainWindow": "./js/mainWindow",
    "authWindow": "./js/authWindow",
    "createWindow": "./js/createWindow",
    "homeTab": "./js/homeTab",
    "fileTab": "./js/fileTab",
    "keyTab": "./js/keyTab",
    "extensionTab": "./js/extensionTab"
}