const {remote, ipcRenderer} = require("electron");
const {resetForm} = require("./components.js")
const path = require("path")
const fs = require("fs")

window.windowName = "createWindow"

if (remote.getGlobal("sql").isOpen()) {
    let redirect = path.join(
        remote.app.getAppPath(), "components/html/main-window.html"
    );
    window.location.href = redirect;
} else {
    fs.exists(remote.getGlobal("config").databasePath, (exists) => {
        if (exists) {
            redirect = path.join(
                remote.app.getAppPath(), "components/html/auth.html"
            );
            window.location.href = redirect;
        }
    }) 
}

$("#start-button").click(function() {
    $("#start").fadeOut(500, "easeInOutQuint", function() {
        $("#start").remove()
    })
})

$("#finish-button").click(function() {
    let redirect = path.join(
        remote.app.getAppPath(), "components/html/main-window.html"
    );
    window.location.href = redirect;
})

let currentTab = 1

$(".form").submit(function() {
    let result = $(this).serializeArray()
    switch (currentTab) {
        case 1:
            remote.getGlobal("config").fullName = result[0].value
            remote.getGlobal("config").email = result[1].value
            ipcRenderer.send("configChanged")
            break;
        case 2:
            let p
            if (result[0].value == "local") {
                p = "./"
            } else {
                p = remote.app.getPath('userData')
            }
            remote.getGlobal("config").rootFolder = p;
            ipcRenderer.send("configChanged")
            let dbpath = remote.getGlobal("config").databasePath
            remote.getGlobal("sql").create_database(dbpath, result[1].value)
        default:
            break;
    }
    resetForm($(this))
    if (currentTab == 2) {
        $(`.step-item#item-2`).addClass("completed")
        $(".tab.current").removeClass("current")
        $(".tab#last").addClass("current")
        return;
    }
    currentTab += 1
    $(".tab.current").removeClass("current")
    $(`.tab:nth-child(${currentTab}n)`).addClass("current")
    $(`.step-item#item-${currentTab-1}`).addClass("completed")
    return false;
})

window.extensionReload()