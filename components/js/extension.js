const {remote, ipcRenderer} = require("electron");
require("./components.js");
remote.getCurrentWebContents().openDevTools();

window.windowName = "extensionTab"

function loadExtensions() {
    /*
    <i class="mi mi-Accept"></i>
    */
    let extensions = remote.getGlobal("extensions")
    for (let [key, value] of Object.entries(extensions)) {
        let newExtension = $(`
        <div class="extension">
            <img class="extension-thumb" src="${value.thumbnail}"/>
            <h2 class="extension-name">${value.name}</h2>
            <h4 class="extension-author">${value.author}</h4>
        </div>
        `)
        if (remote.getGlobal("config").activeExtensions.indexOf(key) != -1) {
            newExtension.append(`<i class="mi mi-Accept"></i>`)
        }
        $("#extensions-grid").append(newExtension)
        newExtension.click(function() {
            let activeExtensions = remote.getGlobal("config").activeExtensions
            let index = activeExtensions.indexOf(key)
            index == -1 ? activeExtensions.push(key) : activeExtensions.splice(index, 1)
            if (index == -1) {
                newExtension.append(`<i class="mi mi-Accept"></i>`)
            } else {
                newExtension.find(`.mi-Accept`).remove()
            }
            remote.getGlobal("config").activeExtensions = activeExtensions
            ipcRenderer.send("configChanged")
            $("#reload").css("display", "block")
        })
    }
    if ($("#extensions-grid").children().length == 0) {
        $("#empty").css("display", "block")
    }
}

loadExtensions()

window.extensionReload()

$("#reload").click(function() {
    ipcRenderer.send("windowReload")
})