const {remote, ipcRenderer} = require("electron");
const {searchElUpdate, ContextMenu, FileExplorer} = require("./components.js");

window.windowName = "keyTab"

const path = require("path")

// remote.getCurrentWebContents().openDevTools();

const LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
const UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const DIGITS = "0123456789"
const SPECIAL_CHARS = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"

/*
<option>Google<span hidden>Youtube Gmail</span></option>
<option>Facebook</option>
<option>Instagram</option>
<option>Twitter</option>
<option>Yahoo!</option>
<option>Wikipedia</option>
<option>Amazon</option>
<option>Reddit</option>
<option>Netflix</option>
<option>Bing</option>
<option>Microsoft<span hidden>Outlook Hotmail</span></option>
<option>Github</option>
<option>Samsung</option>
<option class="default">No option find</option>
*/

const known_accounts = [
    ["Google", "Youtube", "Gmail"],
    ["Facebook"],
    ["Instagram"],
    ["Twitter"],
    ["Yahoo!"],
    ["Amazon"],
    ["Reddit"],
    ["Netflix"],
    ["Bing"],
    ["Microsoft", "Outlook", "Hotmail"],
    ["Github"],
    ["Samsung"]
]

for (let k_account of known_accounts) {
    let main = k_account[0]
    let text = `<div class="option">${main}<span hidden> `
    for (let inner_accounts of k_account.slice(1, k_account.length)) {
        text += inner_accounts
    }
    text += `</span></div>`
    $("#form-accounts>div").append($(text))
    searchElUpdate($("#form-accounts"))
}
$("#form-accounts>div").append(`<option class="default">No option find</option>`)

$("#form-accounts>input").on("change keydown", function() {
    for (let k_account of known_accounts) {
        let regex = new RegExp($(this).val(), "i")
        if (k_account.join("").search(regex) != -1) {
            let img_path = path.join(remote.app.getAppPath(), "components", "icons", "accounts", k_account[0] + ".png")
            $("#account-imagepreview>img").css("display", "initial")
            $("#account-imagepreview>i").css("display", "none")
            $("#account-imagepreview>img").attr("src", img_path)
            break;
        } else {
            $("#account-imagepreview>img").css("display", "none")
            $("#account-imagepreview>i").css("display", "initial")
        }
    }
})

function secureRandomValue() {
    let randomBuffer = new Uint32Array(1);
    window.crypto.getRandomValues(randomBuffer);
    return randomBuffer[0] / (0xffffffff + 1);
}

function secureRandomValueRange(min = 0, max = 1) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(secureRandomValue() * (max - min + 1)) + min;
}

function generateKey(keyLength = 16, chars = LOWER_LETTERS + UPPER_LETTERS + DIGITS + SPECIAL_CHARS) {
    let result = ""
    let charsLength = chars.length
    for (let i = 0; i < keyLength; i++) {
        result += chars[secureRandomValueRange(0, charsLength - 1)]
    }
    return result
}

function showInfo(account) {
    $("#info-accountpassword").attr("type", "password")
    $("#account-info").removeClass("hidden")
    $("#account-name").html(account.name)
    $("#info-accountemail").val(account.email)
    $("#info-accountpassword").val(account.password)
    $("#info-accounturl").val(account.url)
}

function getAccounts() {
    let accounts = remote.getGlobal("sql").get("accounts") || []
    let last = $(`
    <div class="account" id="create-new">
        <div class="acc-img">
            <i class="mi mi-Add"></i>
        </div>
        <div class="acc-info">
            <b class="acc-title">Add a new account</b>
        </div>
    </div>
    `)
    let el = $("#account-list")

    el.html('')

    for (let account of accounts) {
        let newAccount = $(`
        <div class="account">
            <div class="acc-img">
                <img src="" style="display: none;">
                <i class="mi mi-Accounts"></i>
            </div>
            <div class="acc-info">
                <b class="acc-title">${account.name}</b>
                <span class="acc-email">${account.email}</span>
            </div>
        </div>
        `)
        el.append(newAccount)
        newAccount.click(function() {
            showInfo(account)
        })
        for (let k_account of known_accounts) {
            let regex = new RegExp(account.name, "i")
            if (k_account.join("").search(regex) != -1) {
                let img_path = path.join(remote.app.getAppPath(), "components", "icons", "accounts", k_account[0] + ".png")
                newAccount.find(".acc-img>img").css("display", "initial")
                newAccount.find(".acc-img>i").css("display", "none")
                newAccount.find(".acc-img>img").attr("src", img_path)
                break;
            }
        }
    }
    el.append(last)
}

getAccounts()

$("#create-new").click(function() {
    $("#new-account").removeClass("hidden")
    $("#accounts").addClass("hidden")
})

$("#generate-password").click(function() {
    $("#password-generator").removeClass("hidden")
})

$("#generate").click(function() {
    let options = {
        "size": $("#size").val(),
        "lower": $("#lower").prop("checked") ? LOWER_LETTERS : '',
        "upper": $("#upper").prop("checked") ? UPPER_LETTERS : '',
        "digits": $("#digits").prop("checked") ? DIGITS : '',
        "special": $("#special").prop("checked") ? SPECIAL_CHARS : ''
    }
    let password = generateKey(options.size, options.lower + options.upper + options.digits + options.special)
    $("#result-password").val(password)
    $("#result-password").trigger("change")
})

$("#form-generatepassword").submit(function() {
    $("#password-generator").addClass("hidden")
    let password = $("#result-password").val()
    $("#form-generatepassword").trigger("reset")
    $("#form-accountpassword").val(password)
    $("#form-accountpassword").trigger("change")
})

$("#generate-cancel").click(function() {
    $("#password-generator").addClass("hidden")
    $("#form-generatepassword").trigger("reset")
})

$("#create-cancel").click(function() {
    $("#new-account").addClass("hidden")
    $("#accounts").removeClass("hidden")
    $("#new-account").trigger("reset")
})

$("#new-account").submit(function() {
    let account = {
        name: $("#form-accountname").val(),
        email: $("#form-accountemail").val(),
        url: $("#form-accounturl").val(),
        password: $("#form-accountpassword").val()
    }
    let accounts = remote.getGlobal("sql").get("accounts") || []

    accounts.push(account)

    remote.getGlobal("sql").set("accounts", accounts)
    $("#create-cancel").click()
    getAccounts()
})

window.extensionReload()