const {remote, ipcRenderer} = require("electron");
const fs = require("fs")
const path = require("path")
require("./components.js")

window.windowName = "authWindow"

if (remote.getGlobal("sql").isOpen()) {
    let redirect = path.join(
        remote.app.getAppPath(), "components/html/main-window.html"
    );
    window.location.href = redirect;
} else {
    fs.exists(remote.getGlobal("config").databasePath, (exists) => {
        if (!exists) {
            redirect = path.join(
                remote.app.getAppPath(), "components/html/create.html"
            );
            window.location.href = redirect;
        }
    }) 
}

$(".form").submit(function() {
    let password = $(this).serializeArray()[0].value
    let dbpath = remote.getGlobal("config").databasePath
    $(this).find("input[name='password']").val("")
    $(this).find("input[name='password']").removeClass("form-error")
    $(this).find(".form-inputerror").removeClass("show")
    $(this).find(".form-submit").attr("disabled", true)
    remote.getGlobal("sql").open_database(dbpath, password).then((d) => {
        let redirect = path.join(
            remote.app.getAppPath(), "components/html/main-window.html"
        );
        window.location.href = redirect;
    }).catch((err) => {
        $(this).find("input[name='password']").addClass("error")
        $(this).find(".error").addClass("show")
        $(this).find(".form-submit").attr("disabled", false)
    })
    return false;
})

window.extensionReload()